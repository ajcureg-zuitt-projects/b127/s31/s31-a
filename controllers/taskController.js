// Controllers contain the functions and business logic of our Express js application
// Meaning all the operation it can do will be placed in this file

// Allows us to use the contents of the "task.js" file in the models folder
const Task = require('../models/task');

// Controller function for getting all the tasks

module.exports.getAllTasks = () => {

	// The "return" statement, returns the result of the Mongoose method "find" back to the "taskRoute.js" file which invokes this function when the "/tasks" routes is accessed
	// The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client
	return Task.find({}).then(result => {
		// The "return" her, returns the result of the MongoDB query to the "result" parameter defined in the "then" method
		return result;
	})
}


// The request body coming from the client was passed from the "taskRoute.js" file via the "req.body" as an argument and is renamed 'requestBody' parameter in the controller file
module.exports.createTask = (requestBody) => {
	// Create a task object based on the Mongoose model taskController
	let newTask = new Task({
		name: requestBody.name
	})

	// Saves the newly created "newTask" object in the MongoDB database
	// .then method waits until the task is stored/error
	// .then method will accept 2 arguments:
		// First parameter will store the result returned by the save method
		// Second parameter will store the error object
	// Compared to using a callback function on Mongoose methods discussed in the previous session, the first parameter stores the result and the error is stored in the second parameter.
	return newTask.save().then((task, error) => {
		// if an error is encountered, the "return" statement will prevent any other line or code below it and within the same code block from executing
		// Since the following return statement is nested within the "then" method chained to "save" method, they do not prevent each toher from executing code.
		if(error){
			console.log(error);
			return false;
		}else{
			return task;
		}
	})

}

// Delete task
// Business Logic
/*
1. Look for the task with the corresponding id provided in the URL/route
2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err)
			return false;
		}else{
			// Delete successful, returns the removed task object back to the client
			return removedTask;
		}
	})
}


// Updating a task
// Business Logic
/*
1. Get the task with the id using the method "findById"
2. Replace the task's name returned from the database with the "name" property from the request body
3. Save the task
*/

module.exports.updateTask = (taskId, newCOntent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(err)
			return false;
		}

		// Results of the "findById" method will be stored in tmedyo ne "result" parameter
		// It's "name" property will be reassigned the value of the "name" received form the request body
		result.name = newCOntent.name;

		return result.save().then((updatedTask, saveErr) =>{
			if(saveErr){
				console.log(saveErr)
				return false;
			}else{
				// Update successfully, returns the updated task object back to the client
				return updatedTask;
			}
		})
	})
}

/*

Activity:
1. Create a route for getting a specific task.
2. Create a controller function for retrieving a specific task.
3. Return the result back to the client/Postman.
4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
5. Create a route for changing the status of a task to "complete".
6. Create a controller function for changing the status of a task to "complete".
7. Return the result back to the client/Postman.
8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
9. Create a git repository named S31.
10. Initialize a local git repository, add the remote link and push to git with the commit message of s31 Activity.
11. Add the link in Boodle named Express Js Modules, Parameterized Routes.

*/


/* s31 Activity: */

// 2. Create a controller function for retrieving a specific task.
// 3. Return the result back to the client/Postman.
// 4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.

module.exports.getOneTask = (chosenTaskId) => {
	return Task.findById(chosenTaskId).then((resultTask, error) => {
		if(error){
			console.log(error)
			return false;
		}else{
			return resultTask;
		}
	})
}


// 6. Create a controller function for changing the status of a task to "complete".
// 7. Return the result back to the client/Postman.
// 8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.

module.exports.updateTaskStatus = (chosenTaskId, updatedStatus) => {
	return Task.findById(chosenTaskId).then((resultTask, error) => {
		if (error) {
			console.log(error);
			return false;
		}

		resultTask.status = updatedStatus.status;

		return resultTask.save().then((updatedStatus, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
				return false;
			}
			else {
				return updatedStatus;
			}
		})
	})
}